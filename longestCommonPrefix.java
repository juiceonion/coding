/**
 * 14. 最长公共前缀
 */
public class longestCommonPrefix {
    public String longestCommonPrefix(String[] strs) {
        StringBuilder longestCommonPrefix = new StringBuilder();
        if (strs == null || strs.length == 0) {
            return longestCommonPrefix.toString();
        }

        int firstLen = strs[0].length();
        for (int i = 0; i < firstLen; i++) {
            char firstLetter = strs[0].charAt(i);
            for (int j = 0; j < strs.length; j++) {
                if (strs[j].length() <= i || firstLetter != strs[j].charAt(i)) {
                    return longestCommonPrefix.toString();
                }
            }
            longestCommonPrefix.append(firstLetter);
        }
        return longestCommonPrefix.toString();
    }
}
